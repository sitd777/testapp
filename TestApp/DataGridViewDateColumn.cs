﻿using System;
using System.Windows.Forms;

namespace TestApp
{
    class DataGridViewDateColumn : DataGridViewTextBoxColumn
    {
        public String DateFormat { get; set; }

        public DataGridViewDateColumn()
        {
            this.DateFormat = "dd.MM.yyyy";
        }

        protected override void OnDataGridViewChanged()
        {
            base.OnDataGridViewChanged();
            if (this.DataGridView != null) this.DataGridView.CellFormatting += new DataGridViewCellFormattingEventHandler(DataGridView_CellFormatting);
        }

        void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            int self_index = dg.Columns.IndexOf(this);
            // Check column index
            if (e.ColumnIndex == self_index)
            {
                // Check existing data
                if (e.Value == null || e.Value == System.DBNull.Value)
                {
                    e.Value = "";
                }
                else
                {
                    e.Value = ((DateTime)e.Value).ToString(this.DateFormat);
                }
            }
        }
    }
}
