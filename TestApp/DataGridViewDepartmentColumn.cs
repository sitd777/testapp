﻿using System.Data;
using System.Windows.Forms;

namespace TestApp
{
    class DataGridViewDepartmentColumn : DataGridViewTextBoxColumn
    {
        private SQLiteTable _tbl = null;

        public DataGridViewDepartmentColumn(SQLiteTable tbl)
        {
            this._tbl = tbl;
        }

        protected override void OnDataGridViewChanged()
        {
            base.OnDataGridViewChanged();
            if (this.DataGridView != null) this.DataGridView.CellFormatting += new DataGridViewCellFormattingEventHandler(DataGridView_CellFormatting);
        }

        void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            int self_index = dg.Columns.IndexOf(this);
            // Check column index
            if (e.ColumnIndex == self_index)
            {
                // Check existing data
                if (e.Value == null || e.Value == System.DBNull.Value)
                {
                    e.Value = "";
                }
                else
                {
                    // Find department by ID
                    DataView dv = new DataView(this._tbl.DataTable, "id = " + e.Value, "", DataViewRowState.CurrentRows);
                    if (dv.Count == 0)
                    {
                        // Not found
                        e.Value = "";
                    }
                    else
                    {
                        // Found
                        e.Value = ((DataRowView)dv[0]).Row["name"].ToString();
                    }
                    // Cleanup
                    dv.Dispose();
                }
            }
        }
    }
}
