﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace TestApp
{
    public partial class FormList : Form
    {
        // Database file location
        protected String _file = "";

        // Database conection
        protected SQLiteConnection _conn;

        // Employees data table
        protected SQLiteTable _tbl;

        // Departments data table
        protected SQLiteTable _tblDepartments;

        // DataView with full data
        protected DataView _dv;

        // Records on page
        protected int _recOnPage = 20;

        public FormList()
        {
            InitializeComponent();

            // Init database
            this._file = Application.CommonAppDataPath + Path.DirectorySeparatorChar + "test.sl3";
            if (!File.Exists(this._file)) File.Copy(Application.StartupPath + Path.DirectorySeparatorChar + "test.sl3", this._file);

            this._conn = new SQLiteConnection("Data Source=" + this._file);
            this._conn.Open();
            this._tbl = new SQLiteTable("employees", this._conn);
            this._tblDepartments = new SQLiteTable("departments", this._conn);
            this._dv = new DataView(this._tbl.DataTable, "", "name ASC", DataViewRowState.CurrentRows);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Configure DataGrid
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.Columns.Clear();

            // Prepare picture column
            DataGridViewImageBase64Column picture = new DataGridViewImageBase64Column(new System.Drawing.Size(80, 80));
            picture.Name = picture.DataPropertyName = "picture";
            picture.HeaderText = "Фото";
            picture.Width = 50;
            this.dataGridView1.Columns.Add(picture);

            // Prepare name column
            DataGridViewTextBoxColumn name = new DataGridViewTextBoxColumn();
            name.Name = name.DataPropertyName = "name";
            name.HeaderText = "Ф.И.О.";
            this.dataGridView1.Columns.Add(name);

            // Prepare birth date column
            DataGridViewDateColumn birthdate = new DataGridViewDateColumn();
            birthdate.DateFormat = "dd.MM.yyyy";
            birthdate.Name = birthdate.DataPropertyName = "birthdate";
            birthdate.HeaderText = "Дата рождения";
            this.dataGridView1.Columns.Add(birthdate);

            // Prepare department column
            DataGridViewDepartmentColumn department = new DataGridViewDepartmentColumn(this._tblDepartments);
            department.Name = department.DataPropertyName = "department_id";
            department.HeaderText = "Отдел";
            this.dataGridView1.Columns.Add(department);

            // Init paged data
            bindingNavigator1.BindingSource = bindingSource1;
            bindingSource1.CurrentChanged += new System.EventHandler(bindingSource1_CurrentChanged);
            bindingSource1.DataSourceChanged += new System.EventHandler(bindingSource1_CurrentChanged);
            this.dataChanged();
        }

        private void dataChanged()
        {
            // Update pagination
            bindingSource1.DataSource = new PageOffsetList(this._tbl, this._recOnPage);
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {
            // Prepare data source with paged data
            if (bindingSource1.Current == null) return;
            int offset = (int)bindingSource1.Current;
            if(dataGridView1.DataSource is DataView) this._dv.Sort = ((DataView)dataGridView1.DataSource).Sort;
            DataTable tbl = this._dv.ToTable().AsEnumerable().Skip(offset).Take(this._recOnPage).CopyToDataTable();
            DataView dv = new DataView(tbl, "", this._dv.Sort, DataViewRowState.CurrentRows);
            dataGridView1.DataSource = dv;
        }

        class PageOffsetList : System.ComponentModel.IListSource
        {
            protected SQLiteTable _tbl;
            protected int _recOnPage = 1;
            public PageOffsetList(SQLiteTable tbl, int recOnPage)
            {
                this._tbl = tbl;
                this._recOnPage = recOnPage;
            }

            public bool ContainsListCollection { get; protected set; }

            public System.Collections.IList GetList()
            {
                var pageOffsets = new List<int>();
                for (int offset = 0; offset < _tbl.DataTable.Rows.Count; offset += this._recOnPage)
                {
                    pageOffsets.Add(offset);
                }
                return pageOffsets;
            }
        }

        private void recordCreate(object sender, EventArgs e)
        {
            // Prepare edit form
            DataRow dr = this._tbl.DataTable.NewRow();
            FormEdit frm = new FormEdit(dr, this._tblDepartments);

            if(frm.ShowDialog() == DialogResult.OK)
            {
                // Insert record
                this._tbl.DataTable.Rows.Add(dr);
                this._tbl.Update();
                this.dataChanged();
            }
            frm.Dispose();
        }

        private DataRow recordFind()
        {
            // Check record is selected
            if (this.dataGridView1.SelectedRows.Count == 0) return null;
            
            // Get row ID from cloned data table
            String id = ((DataRowView)this.dataGridView1.SelectedRows[0].DataBoundItem).Row["id"].ToString();
            
            // Search source data row
            DataView dv = new DataView(this._tbl.DataTable, "id = " + id, "", DataViewRowState.CurrentRows);
            if (dv.Count == 0) return null;

            return ((DataRowView)dv[0]).Row;
        }

        private void recordEdit(object sender, EventArgs e)
        {
            // Get record
            DataRow dr = this.recordFind();
            if (dr == null) return;

            // Show edit form
            FormEdit frm = new FormEdit(dr, this._tblDepartments);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                // Update record
                this._tbl.Update();
                this.dataChanged();
            }
            frm.Dispose();
        }

        private void recordDelete(object sender, EventArgs e)
        {
            // Get record
            DataRow dr = this.recordFind();
            if (dr == null) return;

            if (MessageBox.Show("Удаляем?", "Вопрос:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // Delete record
                dr.Delete();
                this._tbl.Update();
                this.dataChanged();
            }
        }

        private void dataGridView1_Sorted(object sender, EventArgs e)
        {
            // Update paged data on sorting
            bindingSource1_CurrentChanged(sender, e);
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Select item on right mouse click
            if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
                if(hti.RowIndex > -1)
                {
                    this.dataGridView1.ClearSelection();
                    this.dataGridView1.Rows[hti.RowIndex].Selected = true;
                }
            }
        }

        private void toolStripTextBoxRecsOnPage_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            if(!Int32.TryParse(this.toolStripTextBoxRecsOnPage.Text, out i))
            {
                this.toolStripTextBoxRecsOnPage.Text = ((Int32)this._recOnPage).ToString();
            }
            else
            {
                this._recOnPage = i;
                this.dataChanged();
            }
        }
    }
}
