﻿namespace TestApp
{
    partial class FormEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEdit));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonPictureClear = new System.Windows.Forms.Button();
            this.buttonPictureBrowse = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelPicture = new System.Windows.Forms.Label();
            this.comboBoxDepartment = new System.Windows.Forms.ComboBox();
            this.labelDepartment = new System.Windows.Forms.Label();
            this.dateBirthdate = new System.Windows.Forms.DateTimePicker();
            this.labelBirthdate = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.openFileDialogPicture = new System.Windows.Forms.OpenFileDialog();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonPictureClear);
            this.groupBox1.Controls.Add(this.buttonPictureBrowse);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.labelPicture);
            this.groupBox1.Controls.Add(this.comboBoxDepartment);
            this.groupBox1.Controls.Add(this.labelDepartment);
            this.groupBox1.Controls.Add(this.dateBirthdate);
            this.groupBox1.Controls.Add(this.labelBirthdate);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.labelName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(458, 246);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Работник";
            // 
            // buttonPictureClear
            // 
            this.buttonPictureClear.Image = global::TestApp.Properties.Resources.cancel;
            this.buttonPictureClear.Location = new System.Drawing.Point(347, 138);
            this.buttonPictureClear.Name = "buttonPictureClear";
            this.buttonPictureClear.Size = new System.Drawing.Size(26, 23);
            this.buttonPictureClear.TabIndex = 11;
            this.buttonPictureClear.UseVisualStyleBackColor = true;
            this.buttonPictureClear.Click += new System.EventHandler(this.buttonPictureClear_Click);
            // 
            // buttonPictureBrowse
            // 
            this.buttonPictureBrowse.Location = new System.Drawing.Point(347, 109);
            this.buttonPictureBrowse.Name = "buttonPictureBrowse";
            this.buttonPictureBrowse.Size = new System.Drawing.Size(26, 23);
            this.buttonPictureBrowse.TabIndex = 10;
            this.buttonPictureBrowse.Text = "...";
            this.buttonPictureBrowse.UseVisualStyleBackColor = true;
            this.buttonPictureBrowse.Click += new System.EventHandler(this.buttonPictureBrowse_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.ErrorImage = global::TestApp.Properties.Resources.noimagefound;
            this.pictureBox1.InitialImage = global::TestApp.Properties.Resources.noimagefound;
            this.pictureBox1.Location = new System.Drawing.Point(223, 109);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(118, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // labelPicture
            // 
            this.labelPicture.Location = new System.Drawing.Point(18, 107);
            this.labelPicture.Name = "labelPicture";
            this.labelPicture.Size = new System.Drawing.Size(159, 23);
            this.labelPicture.TabIndex = 8;
            this.labelPicture.Text = "Фото";
            this.labelPicture.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxDepartment
            // 
            this.comboBoxDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartment.FormattingEnabled = true;
            this.comboBoxDepartment.Location = new System.Drawing.Point(223, 82);
            this.comboBoxDepartment.Name = "comboBoxDepartment";
            this.comboBoxDepartment.Size = new System.Drawing.Size(211, 21);
            this.comboBoxDepartment.TabIndex = 7;
            // 
            // labelDepartment
            // 
            this.labelDepartment.Location = new System.Drawing.Point(19, 80);
            this.labelDepartment.Name = "labelDepartment";
            this.labelDepartment.Size = new System.Drawing.Size(159, 23);
            this.labelDepartment.TabIndex = 6;
            this.labelDepartment.Text = "Отдел";
            this.labelDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateBirthdate
            // 
            this.dateBirthdate.Location = new System.Drawing.Point(223, 56);
            this.dateBirthdate.Name = "dateBirthdate";
            this.dateBirthdate.Size = new System.Drawing.Size(211, 20);
            this.dateBirthdate.TabIndex = 5;
            // 
            // labelBirthdate
            // 
            this.labelBirthdate.Location = new System.Drawing.Point(18, 53);
            this.labelBirthdate.Name = "labelBirthdate";
            this.labelBirthdate.Size = new System.Drawing.Size(159, 23);
            this.labelBirthdate.TabIndex = 2;
            this.labelBirthdate.Text = "Дата рождения";
            this.labelBirthdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(223, 29);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(211, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // labelName
            // 
            this.labelName.ForeColor = System.Drawing.Color.Red;
            this.labelName.Location = new System.Drawing.Point(18, 27);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(159, 23);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Фамилия, Имя, Отчество *";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialogPicture
            // 
            this.openFileDialogPicture.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG;*.JPEG";
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Image = global::TestApp.Properties.Resources.ok;
            this.buttonOk.Location = new System.Drawing.Point(155, 269);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 26);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = " Oк";
            this.buttonOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Image = global::TestApp.Properties.Resources.cancel;
            this.buttonCancel.Location = new System.Drawing.Point(236, 269);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 26);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // FormEdit
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(484, 305);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TestApp - Edit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEdit_FormClosing);
            this.Load += new System.EventHandler(this.FormEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelBirthdate;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxDepartment;
        private System.Windows.Forms.Label labelDepartment;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.DateTimePicker dateBirthdate;
        private System.Windows.Forms.Button buttonPictureClear;
        private System.Windows.Forms.Button buttonPictureBrowse;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelPicture;
        private System.Windows.Forms.OpenFileDialog openFileDialogPicture;
    }
}