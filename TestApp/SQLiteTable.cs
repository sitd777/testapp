﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace TestApp
{
    public class SQLiteTable
    {
        protected SQLiteDataAdapter da = null;
        protected SQLiteCommandBuilder cb = null;
        protected DataTable dt = null;

        public DataTable DataTable
        {
            get { return this.dt; }
        }

        public SQLiteDataAdapter Adapter
        {
            get { return this.da; }
        }

        public SQLiteCommandBuilder CommandBuilder
        {
            get { return this.cb; }
        }

        public void Update()
        {
            this.da.Update(this.dt);
        }

        public SQLiteTable(string tableName, SQLiteConnection conn)
        {
            this.dt = new DataTable();
            this.dt.TableName = tableName;

            this.da = new SQLiteDataAdapter();
            da.SelectCommand = new SQLiteCommand("SELECT * FROM " + tableName, conn);
            this.cb = new SQLiteCommandBuilder(da);
            //da.AcceptChangesDuringUpdate = true;
            da.UpdateCommand = this.cb.GetUpdateCommand();
            da.InsertCommand = this.cb.GetInsertCommand();
            da.DeleteCommand = this.cb.GetDeleteCommand();
            try
            {
                da.Fill(this.dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "DB Error:", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
