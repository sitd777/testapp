﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace TestApp
{
    public partial class FormEdit : Form
    {
        private DataRow _dr;

        private SQLiteTable _tblDepartments;

        public FormEdit(DataRow dr, SQLiteTable tblDepartments)
        {
            InitializeComponent();
            this._dr = dr;
            this._tblDepartments = tblDepartments;
        }

        private void FormEdit_Load(object sender, EventArgs e)
        {
            // Prepare name
            this.textBoxName.Text = this._dr["name"].ToString();

            // Prepare birthdate
            DateTime bd;
            if (DateTime.TryParse(this._dr["birthdate"].ToString(), out bd))
            {
                this.dateBirthdate.Value = bd;
            }

            // Prepare departments
            this.comboBoxDepartment.Items.Clear();
            DataView dv = new DataView(this._tblDepartments.DataTable, "", "name ASC", DataViewRowState.CurrentRows);
            String id = this._dr["department_id"].ToString();
            foreach (DataRowView drv in dv)
            {
                ComboBoxDropDownItem item = new ComboBoxDropDownItem(drv["id"].ToString(), drv["name"].ToString());
                this.comboBoxDepartment.Items.Add(item);
                if (id.Length > 0 && id.Equals(drv["id"].ToString())) this.comboBoxDepartment.SelectedItem = item;
            }

            // Prepare picture
            pictureClear();
            String base64 = this._dr["picture"].ToString();
            if(base64.Length > 0)
            {
                this.pictureBox1.Image = Base64.Base64ToImage(base64);
            }
        }

        private void buttonPictureBrowse_Click(object sender, EventArgs e)
        {
            if(openFileDialogPicture.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialogPicture.FileName);
            }
        }

        private void buttonPictureClear_Click(object sender, EventArgs e)
        {
            pictureClear();
        }

        private void pictureClear()
        {
            pictureBox1.Image = pictureBox1.InitialImage;
        }

        private void FormEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult == DialogResult.OK)
            {
                if (this.textBoxName.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Не заполнено Ф.И.О. работника!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    return;
                }

                // Save name
                this._dr["name"] = this.textBoxName.Text;

                // Save birthdate
                this._dr["birthdate"] = this.dateBirthdate.Value;

                // Save department
                if (this.comboBoxDepartment.SelectedItem == null) this._dr["department_id"] = System.DBNull.Value;
                else this._dr["department_id"] = ((ComboBoxDropDownItem)this.comboBoxDepartment.SelectedItem).Id;

                // Save picture
                if (pictureBox1.Image != pictureBox1.InitialImage) this._dr["picture"] = Base64.ImageToBase64(pictureBox1.Image);
                else this._dr["picture"] = "";
            }
        }
    }
}
