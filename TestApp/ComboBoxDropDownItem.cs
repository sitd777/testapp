﻿using System;

namespace TestApp
{
    class ComboBoxDropDownItem
    {
        public String Id { get; }
        public String Value { get; }

        public ComboBoxDropDownItem(String id, String value)
        {
            this.Id = id;
            this.Value = value;
        }

        public override String ToString()
        {
            return this.Value;
        }
    }
}
