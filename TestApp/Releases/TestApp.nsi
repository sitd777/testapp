!define VER "0.1"
!define APP "TestApp"
!define APPV "TestApp"
!define COMPANY "Microsoft"
!define ICON "TestApp.ico"

!include x64.nsh

Name "${APP}"
Caption "${APP} Setup v.${VER}"
Icon "..\${ICON}"
OutFile "${APPV}_Setup_${VER}.exe"

;--------------------------------

VIProductVersion "${VER}.0.0"
VIAddVersionKey "ProductName" "${APP}"
VIAddVersionKey "CompanyName" "${COMPANY}"
VIAddVersionKey "LegalTrademarks" "${APP} is a trademark of ${COMPANY}"
VIAddVersionKey "LegalCopyright" "Copyright ${COMPANY}"
VIAddVersionKey "FileDescription" "Rdx system dispatcher application"
VIAddVersionKey "FileVersion" "${VER}"

;--------------------------------

SetDateSave on
SetDatablockOptimize on
CRCCheck on
SilentInstall normal
BGGradient 29294f 000080 FFFFFF
InstallColors 8080FF 000030
XPStyle on

InstallDir "$PROGRAMFILES\${COMPANY}\${APPV}"
InstallDirRegKey HKLM "Software\${COMPANY}\${APPV}" "Install_Dir"

CheckBitmap "${NSISDIR}\Contrib\Graphics\Checks\simple.bmp"

AutoCloseWindow false
ShowInstDetails show

RequestExecutionLevel admin

;--------------------------------

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

#LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
#LoadLanguageFile "${NSISDIR}\Contrib\Language files\Estonian.nlf"
#LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"

;--------------------------------

Section "Install" ; empty string makes it hidden, so would starting with -

  ; write reg info
  DetailPrint "Writing registry..."
  WriteRegStr HKLM SOFTWARE\${COMPANY}\${APPV} "Install_Dir" "$INSTDIR"

  ; write uninstall strings
  ${if} ${RunningX64}
		SetRegView 64
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayName" "${APP}"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "UninstallString" '"$INSTDIR\uninstall.exe"'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayIcon" '"$INSTDIR\${APPV}.exe"'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "Publisher" '${COMPANY}'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayVersion" '${VER}'
		SetRegView 32
	${else}
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayName" "${APP}"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "UninstallString" '"$INSTDIR\uninstall.exe"'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayIcon" '"$INSTDIR\${APPV}.exe"'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "Publisher" '${COMPANY}'
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}" "DisplayVersion" '${VER}'
  ${endif}

  SetOutPath $INSTDIR

	File /r /x *.pdb /x *.vshost.* /x *.history /x *.tpl /x app.publish ..\bin\Release\*

	CreateDirectory "$SMPROGRAMS\${COMPANY}"
	CreateShortCut "$SMPROGRAMS\${COMPANY}\${APPV}.lnk" "$INSTDIR\${APPV}.exe"
	CreateShortCut "$DESKTOP\${APPV}.lnk" "$INSTDIR\${APPV}.exe"

  WriteUninstaller "uninstall.exe"

SectionEnd




;--------------------------------
; Uninstaller

UninstallText "This will uninstall ${APP}. Hit next to continue."
UninstallIcon "..\${ICON}"

Section "Uninstall"

  ${if} ${RunningX64}
		SetRegView 64
		DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}"
		SetRegView 32
	${else}
		DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPV}"
  ${endif}
	
  DeleteRegKey HKLM "SOFTWARE\${COMPANY}\${APPV}"
	SetOutPath $TEMP
	RMDir /r "$INSTDIR"
	
	Delete "$SMPROGRAMS\${COMPANY}\${APPV}.lnk"
	Delete "$DESKTOP\${APPV}.lnk"

SectionEnd