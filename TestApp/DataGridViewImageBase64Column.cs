﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TestApp
{
    class DataGridViewImageBase64Column : DataGridViewImageColumn
    {
        public Size MaxSize { get; }

        private Image _noImage;

        public DataGridViewImageBase64Column(Size maxSize)
        {
            this.MaxSize = maxSize;
            this._noImage = ((Image)Properties.Resources.noimagefound).GetThumbnailImage(this.MaxSize.Width, this.MaxSize.Height, null, IntPtr.Zero);
        }

        protected override void OnDataGridViewChanged()
        {
            base.OnDataGridViewChanged();
            if (this.DataGridView != null) this.DataGridView.CellFormatting += new DataGridViewCellFormattingEventHandler(DataGridView_CellFormatting);
        }

        void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            int self_index = dg.Columns.IndexOf(this);
            // Check column index
            if (e.ColumnIndex == self_index)
            {
                // Check existing data
                if (e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Length == 0)
                {
                    e.Value = this._noImage;
                }
                else
                {
                    Image image = Base64.Base64ToImage(e.Value.ToString());
                    Image thumbnail = image.GetThumbnailImage(this.MaxSize.Width, this.MaxSize.Height, null, IntPtr.Zero);
                    e.Value = thumbnail;
                }
            }
        }
    }
}
