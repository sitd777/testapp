Windows desktop  test application created using Microsoft Visual Studio Community 2015

* Language: C#
* Framework: .Net Framework 4.6
* Database: SQLite v3

Test images: TestApp/img/test_avatars/

Setup file: TestApp/Releases/TestApp_Setup_0.1.exe

Screenshots: TestApp/Screenshots/